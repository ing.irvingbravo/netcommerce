<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Task;
use App\Models\User;
use App\Models\Company;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Task>
 */
class TaskFactory extends Factory
{
    protected $model = Task::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id' => User::all()->random()->id,
            'company_id' => Company::all()->random()->id,
            'name' => fake()->name(),
            'description' => fake()->sentence(15),
            'is_completed' => fake()->boolean(),
            'start_at' => fake()->dateTimeThisMonth()->format('Y-m-d H:i:s'),
            'expired_at' => fake()->dateTimeThisMonth()->format('Y-m-d H:i:s'),
        ];
    }
}
