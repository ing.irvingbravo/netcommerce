<?php

namespace App\Services;

use App\Models\Task;
use App\Repositories\TaskRepository;

class TaskService
{
    protected $TaskRepository;

    public function __construct(TaskRepository $TaskRepository)
    {
        $this->TaskRepository = $TaskRepository;
    }

    public function query($query)
    {
        return $this->TaskRepository->query(Task::class, $query);
    }

    public function store($data)
    {
        return $this->TaskRepository->store($data);
    }
}
