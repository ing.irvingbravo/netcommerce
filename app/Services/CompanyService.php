<?php

namespace App\Services;

use App\Models\Company;
use App\Repositories\CompanyRepository;

class CompanyService
{
    protected $CompanyRepository;

    public function __construct(CompanyRepository $CompanyRepository)
    {
        $this->CompanyRepository = $CompanyRepository;
    }

    public function query($query)
    {
        return $this->CompanyRepository->query(Company::class, $query);
    }
}
