<?php

namespace App\Repositories;

use App\Traits\PaginateRepository;
use App\Models\Task;

class TaskRepository
{
    use PaginateRepository;

    public function store($data)
    {
        $TaskUSer = Task::where([
            ['user_id', $data['user_id']],
            ['is_completed', false]
            ])->count();
            
        if ($TaskUSer < 5) {
            $Task = Task::create($data);

            return ok('Tarea creada correctamente', $Task);

        }else {
            return bad_request('Este usuario no puede tener mas de 5 tareas activas');
        }
        
    }
}
