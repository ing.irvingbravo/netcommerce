<?php

namespace App\Repositories;

use App\Traits\PaginateRepository;

class CompanyRepository
{
    use PaginateRepository;
}
