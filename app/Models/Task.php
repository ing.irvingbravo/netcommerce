<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuid;

class Task extends Model
{
    use HasFactory, Uuid, SoftDeletes;

    public $incrementing = false;

    protected $keyType = 'uuid';

    protected $fillable = [
        'user_id',
        'company_id',
        'name',
        'description',
        'is_completed',
        'start_at',
        'expired_at',
    ];

    protected $hidden = [
        'user_id',
        'deleted_at',
        'created_at',
        'updated_at'
    ];
    protected $with = ['user'];

    public function user()
    {
        return $this->belongsTo(User::class)->select('id','name');
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
